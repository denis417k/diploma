from catboost import CatBoostClassifier
from tqdm import tqdm

from Modeling import Modeling
from data_pipeline import get_labels, make_data
from sklearn.model_selection import train_test_split
from datetime import datetime, timedelta
from Extensions import calc_metrics, train_ensemble, to_x_Y, get_splited
import pandas as pd


def prepare_df(df, df_day):
    df = df.copy()
    df.to_csv('good.csv', sep=';', decimal=',')
    df = make_data(df)
    if df_day is not None:
        df_day = make_data(df_day)
        df_day = df_day[df_day.index >= df.index[0]]
        df_day = df_day.drop('Date', axis=1)
        df_day = df_day.shift(1)
        df_day.rename(columns=lambda x: 'week_' + x, inplace=True)
        w = []
        for index, row in tqdm(df_day.iterrows()):
            for i in range(1, 24):
                new_row = row.copy()
                new_row.name = index + timedelta(hours=i)
                w.append(new_row)
        df_day = df_day.append(w)
        data = pd.concat([df, df_day], axis=1)
        data = data.dropna()
    else:
        data = df

    return data


def train_model(df, df_week, atr_coeff):
    df = prepare_df(df, df_week)
    df['week_day'] = df['week_day'].astype(int)

    train_coeff = 0.9
    train_size = int(df.shape[0] * train_coeff)
    train_df = df[:train_size]
    ensemble_valid_size = int(df.shape[0] * (1 - (1 - train_coeff) * 0.8))
    ensemble_valid_df = df[train_size:ensemble_valid_size]
    test_df = df[ensemble_valid_size:]

    test_df = test_df.drop(['labels'], axis=1, errors='ignore')

    train_signals_df_clean = train_df.drop(['Date', 'Time'], axis=1, errors='ignore')
    ensemble_valid_df_clean = ensemble_valid_df.drop(['Date', 'Time'], axis=1, errors='ignore')

    train_signals_df_clean = get_labels(train_signals_df_clean, atr_coeff=atr_coeff)
    ensemble_valid_df_clean = get_labels(ensemble_valid_df_clean, atr_coeff=atr_coeff)

    ##
    print(
        f'valid 1:{ensemble_valid_df_clean[ensemble_valid_df_clean.labels == 1].shape[0]}, 0: {ensemble_valid_df_clean[ensemble_valid_df_clean.labels == 0].shape[0]}')

    custom_strategy = ['sClose', 'SMA150', 'MACDdiff_21_33_18', 'sMACDdiff_21_33_18', 'ATR']
    ema_strategy = ['EMA150', 'EMA', 'sEMA', 'RSI', 'shortEMA-EMA100', 'EMA-EMA100', 'Close-EMA']
    rsi_strategy = ['sClose', 'sRSI', 'ssRSI', 'RSI', 'sssRSI']
    rsi_only = ['RSI']
    ichimoku_strategy = ['ia-Close', 'ib-Close', 'S4(ia-Close)', 'S3(ia-Close)', 'sIA', 'sIB', 'ia', 'ib']
    macd_strategy = ['MACDdiff', 'sMACDdiff', 'ssMACDdiff']
    macd_only = ['MACDdiff']
    cci_strategy = ['CCI', 'pCCI', 'sCCI', 'ssCCI', 'sssCCI', 'ssssCCI']
    cci_only = ['CCI']
    obv_strategy = ['sOBV', 'ssOBV', 'EMA-OBV', 'sOBV-sEMA']
    kst_strategy = ['kst_sig', 'kst', 'kst_d', 'kst_past']
    # mf_strategy = ['mf']
    tsi_only = ['TSI']
    tsi_strategy = ['TSI', 'sTSI', 'ssTSI', 'TSI_sig']
    tsi_divergence = ['sTSI', 'ssTSI', 'sssTSI', 'sClose', 'ssClose', 'sssClose']
    tsi_shift_strategy = ['TSI', 'sTSI', 'ssTSI']
    atr_only = ['ATR']
    price_action_strategy = ['sClose', 'ssClose', 'sssClose', 'Close-High', 'Close-Low', 'High-Low', 'sssssCCI']
    stoch_strategy = ['Stoch', 'sStoch', 'ssStoch', 'sStoch_sign', 'ssStoch_sign', 'StochDiff']
    stoch_only = ['Stoch']
    stoch_diff_only = ['StochDiff', 'sStochDiff']
    uo_only = ['UO']
    skama_only = ['sKAMA']
    mass_ind_strategy = ['massd', 'massdd', 'massddd', 'EMA', 'sEMA']
    wr_only = ['R']

    ensemble = {'custom_strategy': custom_strategy,
                'rsi_strategy': rsi_strategy,
                'ema_strategy': ema_strategy,
                'ichimoku_strategy': ichimoku_strategy,
                'macd_strategy': macd_strategy,
                'cci_strategy': cci_strategy,
                'obv_strategy': obv_strategy,
                'kst_strategy': kst_strategy,
                'TSI_only_strategy': tsi_only,
                'tsi_strategy': tsi_strategy,
                'tsi_divergence_strategy': tsi_divergence,
                'tsi_shift_strategy': tsi_shift_strategy,
                'atr_only_strategy': atr_only,
                'price_action_strategy': price_action_strategy,
                'stoch_strategy': stoch_strategy,
                'stoch_only_strategy': stoch_only,
                'stoch_diff_only_strategy': stoch_diff_only,
                'ultimate_osc_strategy': uo_only,
                'KAMA_strategy': skama_only,
                'mass_ind_strategy': mass_ind_strategy,
                'wr_only_strategy': wr_only,
                'macd_only_strategy': macd_only,
                'rsi_only_strategy': rsi_only,
                'cci_only_strategy': cci_only}

    (X_train, X_valid, Y_train, Y_valid), (Y_train_long, Y_valid_long), (_, _) = get_splited(train_signals_df_clean,
                                                                                             0.5, shuffle=False)
    X_ensemble_valid, Y_ensemble_valid = to_x_Y(ensemble_valid_df_clean)

    _, _, ensemble, (full_valid, full_ensemble_valid) = train_ensemble(ensemble, X_train, X_valid,
                                                                       X_ensemble_valid, Y_train_long,
                                                                       Y_valid_long, Y_ensemble_valid)

    full_valid['labels'] = Y_valid_long
    full_ensemble_valid['labels'] = Y_ensemble_valid

    #

    full_ensemble = pd.concat([full_valid, full_ensemble_valid], axis=0)
    full_ensemble_x, full_ensemble_y = to_x_Y(full_ensemble)
    ensemble_train_x, ensemble_valid_x, ensemble_train_y, ensemble_valid_y = train_test_split(full_ensemble_x, full_ensemble_y, test_size=0.3, shuffle=False)

    ensemble_model = CatBoostClassifier(iterations=300010, depth=8, best_model_min_trees=1, task_type="CPU",
                                        loss_function='Logloss')

    cat_features = ['week_day']

    ensemble_model.fit(ensemble_train_x, ensemble_train_y, use_best_model=True,
                       eval_set=[(ensemble_valid_x, ensemble_valid_y)],
                       early_stopping_rounds=600, silent=True, plot=False, cat_features=cat_features)

    pred = ensemble_model.predict(ensemble_valid_x)
    calc_metrics(pred, ensemble_valid_y, f'{atr_coeff}_ensemble')

    return ensemble_model, ensemble, test_df, full_ensemble


date_format = '%d/%m/%y %H%M%S'
file = 'data/EURUSD_hour.csv'
file_day = 'data/EURUSD.csv'
leverage = 1
df = pd.read_csv(file, sep=',', decimal='.', dtype={'Time': str})
df_day = pd.read_csv(file_day, sep=',', decimal='.', dtype={'Time': str})

ensemble_model_atr1, ensemble_atr1, test_df, train_atr1 = train_model(df, df_day, 1.)
# ensemble_model_atr11, ensemble_atr11, test_df, train_atr11 = train_model(df, df_week, 1.1)
# ensemble_model_atr12, ensemble_atr12, test_df, train_atr12 = train_model(df, df_week, 1.2)
# ensemble_model_atr13, ensemble_atr13, test_df, train_atr13 = train_model(df, df_week, 1.3)
# ensemble_model_atr14, ensemble_atr14, test_df, train_atr14 = train_model(df, df_week, 1.4)
# ensemble_model_atr15, ensemble_atr15, test_df, train_atr15 = train_model(df, df_week, 1.5)
# ensemble_model_atr2, ensemble_atr2, test_df, train_atr2 = train_model(df, df_week, 2)
# ensemble_model_atr25, ensemble_atr25, test_df, train_atr25 = train_model(df, df_week, 2.5)
# ensemble_model_atr12, ensemble_atr12, test_df, train_atr12 = train_model(df, df_week, 1.2)
# ensemble_model_atr1, ensemble_atr1, test_df, train_atr1 = train_model(df, df_week, 1.)
# ensemble_model_atr3, ensemble_atr3, _, train_atr3 = train_model(df, df_week, 3)
# ensemble_model_atr35, ensemble_atr35, _, train_atr35 = train_model(df, df_week, 3.5)

modeling = Modeling('test_data.csv', leverage=leverage)

ensembles = {
             1.: (ensemble_model_atr1, ensemble_atr1, train_atr1),
}


modeling.modeling(ensembles, test_df, date_format)
