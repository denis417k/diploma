from __future__ import (absolute_import, division, print_function,
                        unicode_literals)

import datetime  # For datetime objects
from collections import defaultdict
from backtrader.plot import PlotScheme
from sklearn import metrics
import backtrader as bt
from statistics import mean, StatisticsError
import numpy as np
from sklearn.model_selection import train_test_split
import backtrader.analyzers as btanalyzers
import pandas as pd
from Extensions import calc_metrics
from backtraderEx import printTradeAnalysis, ForexSpreadCommissionScheme, BaH
from data_pipeline import get_labels


def to_x_Y(df):
    df = df.copy()
    X = df.loc[:, df.columns != 'labels']
    y = df[['labels']]
    return X, y


def get_splited(train, test_size=0.3, shuffle=False):
    data1 = train.drop(['High', 'Low', 'Volume', 'Open', 'Close', 'Date', 'Time'], axis=1, errors='ignore')

    X = data1.loc[:, data1.columns != 'labels'].values
    y = data1[['labels']].values
    if (test_size == 0):
        X_train, X_test, Y_train, Y_test = (np.array(X), np.array([]), np.array(y), np.array([]))
    else:
        X_train, X_test, Y_train, Y_test = train_test_split(X, y, test_size=test_size, shuffle=shuffle)

    Y_train_long = Y_train.copy()
    Y_train_short = Y_train.copy()
    Y_test_long = Y_test.copy()
    Y_test_short = Y_test.copy()

    Y_train_long[Y_train_long == -1] = 0
    Y_train_short[Y_train_short == 1] = 0
    Y_train_short[Y_train_short == -1] = 1

    Y_test_long[Y_test_long == -1] = 0
    Y_test_short[Y_test_short == 1] = 0
    Y_test_short[Y_test_short == -1] = 1

    return (X_train, X_test, Y_train, Y_test), (Y_train_long, Y_test_long), (Y_train_short, Y_test_short)


class TestStrategy(bt.Strategy):

    def __init__(self, model_data, filter=None, m_t=1, date_format='%d/%m/%y', leverage=1):
        self._modeling_data = model_data
        self.filter = filter
        self.leverage = leverage
        self.window = defaultdict(lambda: [])
        self.window_size = 2500
        self.max_len = 0
        self.model_type = m_t
        self.free = 0.95
        self.want_to_trade = self.broker.cash * 0.05
        self.date_format = date_format
        self.periods = []
        self.order = None
        self.buyprice = None
        self.buycomm = None
        self.stata = {
            "long_average_profit": [], "short_average_profit": [], "average_profit": []
        }

    def notify_order(self, order):
        if order.status in [order.Submitted, order.Accepted]:
            # Buy/Sell order submitted/accepted to/by broker - Nothing to do
            return

        # Check if an order has been completed
        # Attention: broker could reject order if not enough cash
        if order.status in [order.Completed]:
            open_price = self.open_stat['price']
            close_price = order.executed.price
            portfolio_open = self.open_stat['portfolio']
            portfolio_close = self.broker.cash
            profitPc = (portfolio_close / portfolio_open - 1) * 100
            profitAbs = portfolio_close - portfolio_open
            comm = order.executed.comm * 2
            profitAbsWithoutSpread = profitAbs + comm
            if order.isbuy() and (round(open_price, 7) != round(close_price, 7)):
                self.buyprice = order.executed.price
                self.buycomm = comm

                self.stata['short_average_profit'].append(profitPc)
                self.stata['average_profit'].append(profitPc)

                self.log(
                    'BUY EXECUTED, Price: %.5f, Comm %.5f, Profit %.5f, ProfitWithoutSpread: %.5f' %
                    (close_price,
                     comm,
                     profitAbs, profitAbsWithoutSpread))
            elif round(open_price, 7) != round(close_price, 7):  # Sell
                self.stata['long_average_profit'].append(profitPc)
                self.stata['average_profit'].append(profitPc)

                self.log('SELL EXECUTED, Price: %.5f, Comm %.5f, Profit %.5f, ProfitWithoutSpread: %.5f' %
                         (close_price,
                          comm,
                          profitAbs, profitAbsWithoutSpread))

            dt = self.datas[0].datetime.datetime(0)
            holdPeriod = (dt - self.startHold).seconds / (60 * 60)
            self.periods.append(holdPeriod)

            self.bar_executed = len(self)

        elif order.status in [order.Canceled, order.Margin, order.Rejected]:
            self.log('Order Canceled/Margin/Rejected')

        # Write down: no pending order
        self.order = None

    def log(self, txt, dt=None):
        ''' Logging function fot this strategy'''
        dt = dt or self.datas[0].datetime.datetime()
        print('%s, %s' % (dt.isoformat(), txt))

    def next(self):
        date = self.data.datetime.datetime()
        index = date.strftime(self.date_format)

        if self.data.tick_close is None:
            return

        predicted = {}
        selected_atr = None
        price = None
        for atr in self._modeling_data:

            df = self._modeling_data[atr][1].set_index('Date')
            model_long = self._modeling_data[atr][0]

            if (self.window[atr] is not None) and (len(self.window[atr]) >= self.window_size):
                old_df = self._modeling_data[atr][2]
                windowDf = pd.DataFrame(self.window[atr])
                new_df = pd.concat([old_df, windowDf], axis=0)
                (X_train, X_valid, Y_train, Y_valid), (Y_train_long, Y_valid_long), (_, _) = get_splited(new_df, 0.3,
                                                                                                         shuffle=False)
                model_long.fit(X_train, Y_train, use_best_model=True,
                               eval_set=[(X_valid, Y_valid)],
                               early_stopping_rounds=500, silent=True, plot=False)
                self.window[atr] = []
                self._modeling_data[atr] = (
                self._modeling_data[atr][0], self._modeling_data[atr][1], new_df[self.window_size:])
                print(f'{atr}_retrain: {metrics.accuracy_score(model_long.predict(X_valid), Y_valid)}')

            selected = df.loc[index]
            nextL = df.loc[index:]
            if nextL.shape[0] < 2:
                return
            next = nextL.iloc[1]

            data_to_pred = pd.DataFrame([selected, selected])
            data_to_pred['week_day'] = data_to_pred['week_day'].astype(int)

            self.window[atr].append(selected.drop(['Date', 'Time', 'calc_atr'], errors='ignore'))
            d = data_to_pred.drop(['Date', 'Time', 'labels', 'calc_atr'],
                                  axis=1,
                                  errors='ignore')

            predict_long = model_long.predict(d)[-1]

            price = next['Open']
            selected_atr = selected['calc_atr']
            predicted[atr] = predict_long

        if self.position:
            return

        cash = self.broker.cash
        count_to_buy = int(cash / self.data.tick_close * self.free) * self.leverage

        signalsToBuy = [i for i in predicted if predicted[i] == 1]
        signalsToSell = [i for i in predicted if predicted[i] == 0]

        if len(signalsToBuy) > len(signalsToSell):
            stop_loss = price - selected_atr * max(signalsToBuy)
            take_profit = price + selected_atr * max(signalsToBuy)
            predict_long = 1
            print(
                f'max atr:{max(signalsToBuy)},min atr:{min(signalsToBuy)}, decision:{predict_long}, signals: {predicted}')
        else:
            stop_loss = price + selected_atr * max(signalsToSell)
            take_profit = price - selected_atr * max(signalsToSell)
            predict_long = 0
            print(
                f'max atr:{max(signalsToSell)},min atr:{min(signalsToSell)}, decision:{predict_long}, signals: {predicted}')

        if predict_long == 1:
            self.order = self.buy_bracket(size=count_to_buy, price=price, exectype=bt.Order.Market,
                                          limitprice=take_profit,
                                          stopprice=stop_loss)
            self.log('BUY CREATE: %.5f, STOPLOSS: %.5f, TAKEPROFIT: %.6f, SIZE: %d, PORTFOLIO: %.5f' % (
                price, stop_loss, take_profit, count_to_buy, self.broker.cash),
                     self.datas[0].datetime.datetime() + datetime.timedelta(hours=1))
            self.startHold = date
            self.open_stat = {'price': price, 'amount': count_to_buy, 'portfolio': cash}
        if predict_long == 0:
            self.order = self.sell_bracket(size=count_to_buy, price=price, exectype=bt.Order.Market,
                                           limitprice=take_profit,
                                           stopprice=stop_loss)
            self.log('SELL CREATE: %.5f, STOPLOSS: %.5f, TAKEPROFIT: %.6f, SIZE: %d, PORTFOLIO: %.5f' % (
                price, stop_loss, take_profit, count_to_buy, self.broker.cash),
                     self.datas[0].datetime.datetime() + datetime.timedelta(hours=1))
            self.startHold = date
            self.open_stat = {'price': price, 'amount': count_to_buy, 'portfolio': cash}


class Modeling:
    def __init__(self, test_temp, leverage=1):
        self.test_temp = test_temp
        self.leverage = leverage

    def modeling(self, ensembles, test_data, date_format):
        test_data = test_data.copy()
        test_data.to_csv(self.test_temp, sep=';', decimal='.')
        print(f'Start Date: {test_data["Date"].iloc[0]}')

        col = test_data.columns
        cerebro = bt.Cerebro(stdstats=False)
        ohlc = np.array(
            [col.get_loc('Date'), col.get_loc('Open'), col.get_loc('High'), col.get_loc('Low'), col.get_loc('Close'),
             col.get_loc('Volume')]) + 1

        input_df = test_data.drop(['Open', 'High', 'Low', 'Close', 'Volume', 'Date', 'Time'], axis=1, errors='ignore')

        model_data = {}
        for atr in ensembles:
            ensemble_df = pd.DataFrame()

            ensemble = ensembles[atr][1]
            ensemble_model = ensembles[atr][0]
            train_already_df = ensembles[atr][2]
            for name in ensemble:
                list_of_ind, model = ensemble[name]
                data = input_df[list_of_ind]
                pred = model.predict_proba(data)
                ensemble_df[name] = pred[:, 0]

            ensemble_df.index = test_data.index
            ensemble_df = pd.concat([ensemble_df, test_data], axis=1)
            ensemble_df['Date'] = test_data['Date']
            ensemble_df['Open'] = test_data['Open']
            ensemble_df['Close'] = test_data['Close']
            ensemble_df['calc_atr'] = test_data['atr']

            ensemble_df = get_labels(ensemble_df, atr)
            model_data[atr] = (ensemble_model, ensemble_df, train_already_df)

            x, y = to_x_Y(ensemble_df.drop(['Date', 'Time', 'calc_atr'], axis=1, errors='ignore'))
            calc_metrics(ensemble_model.predict(x), y, f"{atr}:TEST METRICS")

        cerebro.addstrategy(TestStrategy, model_data, date_format=date_format, leverage=self.leverage)

        # Create a Data Feed
        data = bt.feeds.GenericCSVData(
            dataname=self.test_temp,
            adjclose=False,
            dtformat=date_format,
            timeframe=bt.TimeFrame.Minutes,
            compression=60,
            reverse=False,
            separator=';',
            datetime=ohlc[0], open=ohlc[1], high=ohlc[2], low=ohlc[3], close=ohlc[4], volume=ohlc[5])

        # data.plotinfo.plot = False
        # Add the Data Feed to Cerebro
        cerebro.adddata(data)

        # Set our desired cash start
        cerebro.broker.setcash(1000)
        start_cash = cerebro.broker.getvalue()
        # Print out the starting conditions
        print('Starting Portfolio Value: %.2f' % start_cash)

        # Spread
        comminfo = ForexSpreadCommissionScheme(spread=0, leverage=self.leverage)
        cerebro.broker.addcommissioninfo(comminfo)

        # cerebro.addobserver(bt.observers.Value)
        cerebro.addobserver(bt.observers.BuySell, barplot=True, bardist=0.)

        cerebro.addanalyzer(btanalyzers.SharpeRatio, _name='mysharpe')
        cerebro.addanalyzer(btanalyzers.DrawDown, _name='dd')
        cerebro.addanalyzer(bt.analyzers.TradeAnalyzer, _name="ta")
        cerebro.addanalyzer(bt.analyzers.Calmar, _name="calmar")
        # Run over everything
        start = cerebro.run()
        thestrat = start[0]

        final_cash = cerebro.broker.getvalue() / start_cash * 100
        # Print out the final result
        sharp = thestrat.analyzers.mysharpe.get_analysis()["sharperatio"]
        print('Final Portfolio Value: %.2f' % final_cash)
        print('Sharpe Ratio:', sharp)
        print('DD:', thestrat.analyzers.dd.get_analysis())
        print('Calmar:', thestrat.analyzers.calmar.calmar)

        if cerebro.broker.getvalue() == start_cash:
            return None
        try:
            print(f'Average period: {mean(thestrat.periods)}')
        except StatisticsError:
            print('EMPTY')

        if len(thestrat.stata['long_average_profit']) != 0:
            long_average_profit = mean(thestrat.stata['long_average_profit'])
            good = len([i for i in thestrat.stata['long_average_profit'] if i > 0])
            bad = len([i for i in thestrat.stata['long_average_profit'] if i < 0])
            print(f'Long Accuracy: {good / (good + bad)}')
            print(f'Long Average Profit: {long_average_profit}')
        if len(thestrat.stata['short_average_profit']) != 0:
            short_average_profit = mean(thestrat.stata['short_average_profit'])
            good = len([i for i in thestrat.stata['short_average_profit'] if i > 0])
            bad = len([i for i in thestrat.stata['short_average_profit'] if i < 0])
            print(f'Short Accuracy: {good / (good + bad)}')
            print(f'Short Average Profit: {short_average_profit}')
        average_profit = mean(thestrat.stata['average_profit'])

        print(f'Average Profit: {average_profit}')

        printTradeAnalysis(thestrat.analyzers.ta.get_analysis())

        plot_scheme = PlotScheme()
        plot_scheme.volume = False

        cerebro.plot(iplot=False, dpi=450, style='candlestick')

        bah(self.test_temp, test_data, date_format)

        return sharp


def bah(test_temp, df, date_format):
    print("BAH")
    # BAH
    # Create a cerebro entity
    cerebro = bt.Cerebro(stdstats=False)

    # Add a strategy
    cerebro.addstrategy(BaH)

    col = df.columns

    ohlc = np.array(
        [col.get_loc('Date'), col.get_loc('Open'), col.get_loc('High'), col.get_loc('Low'), col.get_loc('Close'),
         col.get_loc('Volume')]) + 1
    # Create a Data Feed
    data = bt.feeds.GenericCSVData(
        dataname=test_temp,
        adjclose=False,
        dtformat=date_format,
        # fromdate=start_param_date,
        reverse=False,
        separator=';',
        datetime=ohlc[0], open=ohlc[1], high=ohlc[2], low=ohlc[3], close=ohlc[4], volume=ohlc[5])

    # Add the Data Feed to Cerebro
    df['Date'] = pd.to_datetime(df['Date'], format=date_format)
    df = df.set_index('Date')
    # data.plotinfo.plot = False
    cerebro.adddata(data)
    start_cash = df.iloc[0]['Close']
    cerebro.broker.setcash(start_cash)

    print('Starting Portfolio Value: %.2f' % start_cash)

    # cerebro.addobserver(bt.observers.Value)
    cerebro.addobserver(bt.observers.BuySell, barplot=True, bardist=0.01)

    cerebro.addanalyzer(btanalyzers.SharpeRatio, _name='mysharpe')
    cerebro.addanalyzer(btanalyzers.DrawDown, _name='dd')
    cerebro.addanalyzer(bt.analyzers.Calmar, _name="calmar")

    start = cerebro.run()
    thestrat = start[0]

    final_cash = cerebro.broker.getvalue() / start_cash * 100
    # Print out the final result
    print('Final Portfolio Value: %.2f' % final_cash)
    print('Sharpe Ratio:', thestrat.analyzers.mysharpe.get_analysis())
    print('DD:', thestrat.analyzers.dd.get_analysis())
    print('Calmar:', thestrat.analyzers.calmar.calmar)

    cerebro.plot(iplot=False, dpi=450)
