from catboost import CatBoostClassifier
import pandas as pd
from pandas import DataFrame
from sklearn import metrics
from sklearn.model_selection import train_test_split


def train_ensemble(ensemble, X_train, X_valid, X_test, Y_train, Y_valid, Y_test) -> (DataFrame, DataFrame):
    ensemble_test_df = pd.DataFrame()
    ensemble_valid_df = pd.DataFrame()
    for name in ensemble:
        list_of_ind = ensemble[name]
        Train, Valid, Test = X_train[list_of_ind], X_valid[list_of_ind], X_test[list_of_ind]
        model = CatBoostClassifier(iterations=300010, depth=7, best_model_min_trees=1, task_type="CPU",
                                   loss_function='Logloss')
        model.fit(Train, Y_train, use_best_model=True, eval_set=[(Valid, Y_valid)],
                  early_stopping_rounds=100, silent=True, plot=False)
        test_pred = model.predict_proba(Test)
        valid_pred = model.predict_proba(Valid)
        ensemble_test_df[name] = test_pred[:, 0]
        calc_metrics(model.predict(Valid), Y_valid, name)
        ensemble_valid_df[name] = valid_pred[:, 0]
        ensemble[name] = (ensemble[name], model)

    ensemble_valid_df.index = X_valid.index
    ensemble_test_df.index = X_test.index


    full_valid = pd.concat([ensemble_valid_df, X_valid], axis=1)
    full_test = pd.concat([ensemble_test_df, X_test], axis=1)
    full_test['labels'] = Y_test
    full_valid['labels'] = Y_valid

    ensemble_valid_df['Close'] = X_valid['Close']
    ensemble_valid_df['Open'] = X_valid['Open']
    ensemble_valid_df['High'] = X_valid['High']
    ensemble_valid_df['Low'] = X_valid['Low']
    ensemble_valid_df['Volume'] = X_valid['Volume']
    ensemble_valid_df['week_day'] = X_valid['week_day']

    ensemble_test_df['Close'] = X_test['Close']
    ensemble_test_df['Open'] = X_test['Open']
    ensemble_test_df['High'] = X_test['High']
    ensemble_test_df['Low'] = X_test['Low']
    ensemble_test_df['Volume'] = X_test['Volume']
    ensemble_test_df['week_day'] = X_test['week_day']

    return ensemble_valid_df, ensemble_test_df, ensemble, (full_valid, full_test)


def calc_metrics(pred, Y_test, model_name, isRegression=False):
    if isRegression:
        print(f'{model_name}: mean absolute error: ', metrics.mean_absolute_error(pred, Y_test))
        print(f'{model_name}: max error: ', metrics.max_error(pred, Y_test))
        print(f'{model_name}: explained variance score: ', metrics.explained_variance_score(pred, Y_test))
        return
    print(f'{model_name}: precision', metrics.precision_score(pred, Y_test))
    print(f'{model_name}: recall', metrics.recall_score(pred, Y_test))
    print(f'{model_name}: accuracy', metrics.accuracy_score(pred, Y_test))
    print(f'{model_name}: f1', metrics.f1_score(pred, Y_test))


def to_x_Y(df, labels_name='labels'):
    df = df.copy()
    X = df.loc[:, df.columns != labels_name]
    y = df[[labels_name]]
    return X, y

def get_splited(train, test_size=0.3, labels_name='labels', shuffle=False):
    data1 = train.copy()

    X, y = to_x_Y(data1, labels_name)

    X_train, X_test, Y_train, Y_test = train_test_split(X, y, test_size=test_size, shuffle=False)

    Y_train_long = Y_train.copy()
    Y_train_short = Y_train.copy()
    Y_test_long = Y_test.copy()
    Y_test_short = Y_test.copy()

    Y_train_long[Y_train_long == -1] = 0
    Y_train_short[Y_train_short == 1] = 0
    Y_train_short[Y_train_short == -1] = 1

    Y_test_long[Y_test_long == -1] = 0
    Y_test_short[Y_test_short == 1] = 0
    Y_test_short[Y_test_short == -1] = 1

    return (X_train, X_test, Y_train, Y_test), (Y_train_long, Y_test_long), (Y_train_short, Y_test_short)