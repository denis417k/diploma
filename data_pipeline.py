"""Import Modules"""

from ta import trend, momentum, volatility, volume
from sklearn.model_selection import train_test_split

import numpy as np
import pandas as pd
import ta
from finta import TA
import warnings
from tqdm import tqdm

warnings.filterwarnings("ignore")


def make_data(df, clean=True, datetime_format='%d/%m/%y %H%M%S'):
    data = df.copy()
    data['Date'] = data['Date'] + ' ' + data['Time'].astype(str)
    data['Datetime'] = data['Date']
    data['Date'] = pd.to_datetime(data['Date'], format=datetime_format)
    data['week_day'] = data['Date'].dt.dayofweek.astype(int)
    data = data.set_index('Date')
    data['Date'] = data['Datetime']
    data = data.drop(['Time', 'Datetime'], axis=1)
    high = data['High'].astype(np.float)
    close = data['Close'].astype(np.float)
    low = data['Low'].astype(np.float)
    volume = data['Volume'].astype(np.float)

    if ('ema' not in set(data.columns)):
        data['atr'] = ta.volatility.average_true_range(high, low, close, 14)
        data['adx'] = ta.trend.adx(high, low, close)
    data['BollingerBandsH'] = ta.volatility.bollinger_hband(close, fillna=False)
    data['BollingerBandsL'] = ta.volatility.bollinger_lband(close, fillna=False)
    data['MACDdiff'] = ta.trend.macd_diff(close)
    data['MACD'] = ta.trend.macd(close)
    data['MACDdiff_21_33_18'] = ta.trend.macd_diff(close, 21, 33, 18)
    data['sMACDdiff_21_33_18'] = data['MACDdiff_21_33_18'] - data['MACDdiff_21_33_18'].shift(1)
    data['ATR'] = ta.volatility.average_true_range(high, low, close, 14)
    data['ADX1'] = ta.trend.adx(high, low, close, n=6)
    data['RSI'] = ta.momentum.rsi(close)
    data['SMA'] = TA.SMA(data, column='Close')
    data['SMA150'] = TA.SMA(data, column='Close', period=150)
    data['EMA100'] = TA.EMA(data, column='Close', period=100)
    data['EMA150'] = TA.EMA(data, column='Close', period=150)
    data['ADX'] = ta.trend.adx(high, low, close)
    data['bb'] = ta.volatility.bollinger_hband_indicator(close)
    data['EMA'] = TA.EMA(data, column='Close')
    data['shortEMA'] = TA.EMA(data, column='Close', period=30)
    data['Stoch'] = ta.momentum.stoch(high, low, close)
    data['StochDiff'] = data.Stoch - ta.momentum.stoch_signal(high, low, close)
    data['Stoch_sign'] = ta.momentum.stoch_signal(high, low, close)
    data['UO'] = ta.momentum.uo(high, low, close)
    data['KAMA'] = ta.momentum.kama(close)
    data['CCI'] = ta.trend.cci(high, low, close)
    data['R'] = ta.momentum.wr(high, low, close)
    data['OBV'] = ta.volume.on_balance_volume(close, volume)
    data['ia'] = ta.trend.ichimoku_a(high, low)
    data['ib'] = ta.trend.ichimoku_a(high, low)
    data['kst_sig'] = ta.trend.kst_sig(close)
    data['kst'] = ta.trend.kst(close)
    data['TSI'] = ta.momentum.tsi(close)
    data['sTSI'] = data.TSI - data.TSI.shift(1)
    data['ssTSI'] = data.TSI.shift(1) - data.TSI.shift(2)
    data['sssTSI'] = data.TSI.shift(2) - data.TSI.shift(3)
    data['TSI_sig'] = TA.EMA(data, column='TSI')
    # data['mf'] = ta.volume.MFIIndicator(high, low, close, volume).money_flow_index()
    data['kst_d'] = data['kst_sig'] - data['kst']
    data['kst_past'] = data['kst_d'].shift(2)
    data['massd'] = ta.trend.mass_index(high, low) - ta.trend.mass_index(high, low).shift(1)
    data['massdd'] = data.massd.shift(1)
    data['massddd'] = data.massd.shift(2)

    data['newEMA'] = TA.EMA(data, column='Close', period=30)
    data['S4'] = data.newEMA - data.SMA
    data['S5'] = data.EMA - data.Close

    data['sMACDdiff'] = data.MACDdiff - data.MACDdiff.shift(1)
    data['ssMACDdiff'] = data.MACDdiff.shift(1) - data.MACDdiff.shift(2)
    data['sATR'] = data.ATR - data.ATR.shift(1)
    data['sKAMA'] = data.KAMA - data.KAMA.shift(1)

    ##
    data['sSMA'] = data.SMA - data.SMA.shift(1)
    data['sClose'] = data.Close - data.Close.shift(1)
    data['ssClose'] = data.Close.shift(1) - data.Close.shift(2)
    data['sssClose'] = data.Close.shift(2) - data.Close.shift(3)
    data['ssssClose'] = data.Close.shift(3) - data.Close.shift(4)
    ##

    data['sRSI'] = data.RSI - data.RSI.shift(1)
    data['ssRSI'] = data.RSI.shift(2) - data.RSI.shift(1)
    data['sssRSI'] = data.RSI.shift(3) - data.RSI.shift(2)
    data['sEMA'] = data.EMA - data.EMA.shift(1)
    data['sStoch'] = data.Stoch - data.Stoch.shift(1)
    data['ssStoch'] = data.Stoch.shift(1) - data.Stoch.shift(2)
    data['sssStoch'] = data.Stoch.shift(2) - data.Stoch.shift(3)
    data['ssssStoch'] = data.Stoch.shift(3) - data.Stoch.shift(4)
    data['sStoch_sign'] = data['Stoch_sign'] - data['Stoch_sign'].shift(1)
    data['ssStoch_sign'] = data['Stoch_sign'].shift(1) - data['Stoch_sign'].shift(2)
    data['sStochDiff'] = data['StochDiff'].shift(1) - data['StochDiff']

    data['sR'] = data.R - data.R.shift(1)
    data['sOBV'] = data.OBV - data.OBV.shift(1)
    data['ssOBV'] = data.OBV - data.OBV.shift(3)

    data['sIA'] = data.ia - data.ia.shift(1)
    data['sIB'] = data.ib - data.ib.shift(1)
    data['pCCI'] = data.CCI.shift(2)
    data['sCCI'] = data.CCI - data.CCI.shift(1)
    data['ssCCI'] = data.CCI.shift(1) - data.CCI.shift(2)

    ##
    data['sssCCI'] = data.CCI.shift(2) - data.CCI.shift(3)
    data['ssssCCI'] = data.CCI.shift(2) - data.CCI.shift(4)
    data['sssssCCI'] = data.CCI.shift(4) - data.CCI.shift(5)
    ##

    data['diffB'] = (data.BollingerBandsH - data.BollingerBandsL).abs()
    data['BC'] = data.BollingerBandsH - data.Close
    data['LC'] = data.BollingerBandsL - data.Close

    ##
    data['Close-EMA'] = data.Close - TA.EMA(data, column='Close', period=50)
    data['EMA-EMA100'] = data.EMA - TA.EMA(data, column='Close', period=100)
    ##

    data['Close-RSI'] = data.Close - data.RSI
    data['EMA-OBV'] = data.EMA - data.OBV
    ##
    data['sOBV-sEMA'] = data.sOBV - data.sEMA
    ##
    data['shortEMA-EMA100'] = data.shortEMA - data.EMA100
    data['ia-Close'] = data.ia - data.Close
    data['ib-Close'] = data.ib - data.Close
    ##
    data['S4(ia-Close)'] = data['ia-Close'].shift(4)
    data['S3(ia-Close)'] = data['ia-Close'].shift(3)
    data['Close-High'] = data.Close - data.High
    data['Close-Low'] = data.Close - data.Low
    data['High-Low'] = data.High - data.Low

    data = data.dropna()
    data = data.reindex(sorted(data.columns), axis=1)

    if clean:
        return data

    print("DataColumns:", data.columns)
    labels = data['labels']
    X = data.loc[:, data.columns != 'labels']
    X = X.values
    y = labels.values

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.25)

    return X_train, y_train, X_test, y_test


def get_labels(df, atr_coeff=2.5):
    if atr_coeff is None:
        return get_labels_static(df)
    df = df.copy()

    high = df.High
    low = df.Low
    openPr = df.Open
    df['labels'] = np.zeros(df.shape[0])
    atr = df['atr']
    for i in tqdm(np.arange(11, df.shape[0] - 1)):
        openPrice = openPr.iloc[i + 1]
        if atr_coeff is not None:
            c = atr.iloc[i] * atr_coeff
        else:
            raise Exception('bad param')

        long_stop = openPrice - c
        long_profit = openPrice + c
        for k in range(i + 1, df.shape[0]):
            if low.iloc[k] < long_stop:
                df.labels.iloc[i] = 0
                break
            if high.iloc[k] > long_profit:
                df.labels.iloc[i] = 1
                break
    return df


def get_labels_static(df):
    df = df.copy()
    df['labels'] = np.sign(df.Close.shift(-1) - df.Close)
    df = df[df.labels != 0]
    df.labels[df.labels == -1] = 0
    return df


